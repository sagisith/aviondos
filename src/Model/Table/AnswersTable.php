<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class QuestionariesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('answers');
        $this->primaryKey('id');
                              
                
        $this->belongsTo('Questions', [
            'foreignKey' => 'question_id'
        ]);
        
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);    
        
        
    }

    public function validationDefault(Validator $validator)
    {
          
    }

}