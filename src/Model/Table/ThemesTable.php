<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ThemesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('themes');
        $this->primaryKey('id');
                       
                
        $this->hasMany('Questions', [
            'foreignKey' => 'theme_id'
        ]);    
        
    }

    public function validationDefault(Validator $validator)
    {
          
    }

}