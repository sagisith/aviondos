<?php

// src/Controller/UsersController.php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;


class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
             
    }
 
    public function isAuthorized($user) {
        //Corregir esto
        return true;
    }
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index','registry','login']);
        
    }
    
    public function registry()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);            
            
            if ($this->Users->save($user)) { 

               $this->Flash->success(__('Registro correcto'));
               return $this->redirect(['action' => 'index']);
           }
           $this->Flash->error(__('Error en el registo.'));
            
        }
        
        //$this->viewBuilder()->layout('nomenu');
        $this->set('user', $user);
    }
    
    public function index(){
        $userId = $this->Auth->user('id');
        $user=null;
        
        if($userId != 0 && $userId != null){
            $user = $this->Users->get($userId);
            
        }
        $this->set('user', $user);
    }
    
    
    
    public function login(){       
        
        if ($this->request->is('post')) {            
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                    return $this->redirect($this->Auth->redirectUrl());
                }            
                $this->Flash->error(__('La combinación de usuario y contraseña no es correcta, intentalo de nuevo'));           
        }
       
    }
}