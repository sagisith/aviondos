<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
               
         $this->belongsToMany('Questionaries', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'questionary_id',
            'joinTable' => 'questionaries_users'
        ]);
        
        $this->hasMany('Answers', [
            'foreignKey' => 'user_id'
        ]);    
        
    }

    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('username', 'El nombre de usuario es obligatorio')
            ->notEmpty('email', 'El email es obligatoria')
            ->notEmpty('password', 'La contraseña es obligatoria');            
    }

}