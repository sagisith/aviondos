<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class QuestionsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('questions');
        $this->primaryKey('id');
        
               
        $this->hasMany('Answers', [
            'foreignKey' => 'question_id'
        ]);    
        
        $this->belongsTo('Themes', [
            'foreignKey' => 'theme_id'
        ]);    
        
        $this->belongsTo('Questionaries', [
            'foreignKey' => 'questionary_id'
        ]);    
        
        
        
    }

    public function validationDefault(Validator $validator)
    {
          
    }

}