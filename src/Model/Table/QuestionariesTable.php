<?php

// src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class QuestionariesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('questionaries');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
                       
                       
         $this->belongsToMany('Users', [
            'foreignKey' => 'questionary_id',
            'targetForeignKey' => 'user_id',
            'joinTable' => 'questionaries_users'
        ]);
        
        $this->hasMany('Questions', [
            'foreignKey' => 'questionary_id'
        ]);    
        
    }

    public function validationDefault(Validator $validator)
    {
          
    }

}